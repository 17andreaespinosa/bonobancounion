package com.elFuturoDelSaber.services.impl;

import com.elFuturoDelSaber.data.dto.EstudianteDto;
import com.elFuturoDelSaber.data.payload.EstudianteForm;

public interface EstudianteService {


    EstudianteDto saveEstudiante(EstudianteForm form);
}
