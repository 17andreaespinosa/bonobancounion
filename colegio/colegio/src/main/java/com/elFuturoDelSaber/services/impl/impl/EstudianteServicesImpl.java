package com.elFuturoDelSaber.services.impl.impl;

import com.elFuturoDelSaber.data.dto.EstudianteDto;
import com.elFuturoDelSaber.data.entity.Estudiante;
import com.elFuturoDelSaber.data.mapper.EstudianteMapper;
import com.elFuturoDelSaber.data.mapper.IMapper;
import com.elFuturoDelSaber.data.payload.EstudianteForm;
import com.elFuturoDelSaber.repository.EstudianteRepository;
import com.elFuturoDelSaber.services.impl.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstudianteServicesImpl implements EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    private EstudianteMapper estudianteMapper;

    @Override
    public EstudianteDto saveEstudiante(EstudianteForm form) {

        Estudiante estudiante1 = new Estudiante();
        estudiante1.setTipoDocumento(form.getTipoDocumento());
        estudiante1.setNumeriIdentidad(form.getNumeriIdentidad());
        estudiante1.setNombres(form.getNombres());
        estudiante1.setApellidos(form.getApellidos());
        estudiante1.setFechaNac(form.getFechaNac());
        estudiante1.setCiudad(form.getCiudad());
        estudiante1.setDireccion(form.getDireccion());
        estudiante1.setEmail(form.getEmail());
        estudiante1.setCelular(form.getCelular());
        estudiante1.setAcudiente(form.getAcudiente());
        estudiante1.setTelFijo(form.getTelFijo());

        Estudiante save = estudianteRepository.save(estudiante1);
        return this.estudianteMapper.map(save);
    }
}
