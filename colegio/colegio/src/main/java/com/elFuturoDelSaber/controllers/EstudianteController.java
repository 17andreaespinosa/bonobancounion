package com.elFuturoDelSaber.controllers;

import com.elFuturoDelSaber.data.dto.EstudianteDto;
import com.elFuturoDelSaber.data.payload.EstudianteForm;
import com.elFuturoDelSaber.services.impl.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping("/save")
    public EstudianteDto saveEstudiante(@RequestBody EstudianteForm estudiante){
    return estudianteService.saveEstudiante(estudiante);
    }


}
