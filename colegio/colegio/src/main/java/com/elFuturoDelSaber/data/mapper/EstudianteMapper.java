package com.elFuturoDelSaber.data.mapper;


import com.elFuturoDelSaber.data.dto.EstudianteDto;
import com.elFuturoDelSaber.data.entity.Estudiante;
import org.springframework.stereotype.Component;

@Component
public class EstudianteMapper implements IMapper<Estudiante, EstudianteDto>{

    public EstudianteDto map(Estudiante in){


        EstudianteDto estudianteDto = new EstudianteDto();
        estudianteDto.setTipoDocumento(in.getTipoDocumento());
        estudianteDto.setNumeriIdentidad(in.getNumeriIdentidad());
        estudianteDto.setNombres(in.getNombres());
        estudianteDto.setApellidos(in.getApellidos());
        estudianteDto.setFechaNac(in.getFechaNac());
        estudianteDto.setCiudad(in.getCiudad());
        estudianteDto.setDireccion(in.getDireccion());
        estudianteDto.setEmail(in.getEmail());
        estudianteDto.setTelFijo(in.getTelFijo());
        estudianteDto.setCelular(in.getCelular());
        estudianteDto.setAcudiente(in.getAcudiente());

        return estudianteDto;
    }




}
