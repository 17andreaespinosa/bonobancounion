package com.elFuturoDelSaber.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EstudianteDto {


    @JsonIgnore
    private Long idEstudiante;
    private String tipoDocumento;
    private long numeriIdentidad;
    private String nombres;
    private String apellidos;
    private String fechaNac;
    private String ciudad;
    private String direccion;
    private String email;
    private long telFijo;
    private long celular;
    private String acudiente;

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public long getNumeriIdentidad() {
        return numeriIdentidad;
    }

    public void setNumeriIdentidad(long numeriIdentidad) {
        this.numeriIdentidad = numeriIdentidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTelFijo() {
        return telFijo;
    }

    public void setTelFijo(long telFijo) {
        this.telFijo = telFijo;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }

    public String getAcudiente() {
        return acudiente;
    }

    public void setAcudiente(String acudiente) {
        this.acudiente = acudiente;
    }
}
