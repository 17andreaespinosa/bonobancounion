package com.elFuturoDelSaber.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "estudiante")
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEstudiante;

    @Column(name = "tipoDoc", nullable = false, length = 50)
    private String tipoDocumento;

    @Column(name = "nId", nullable = false, length = 50 )
    private long numeriIdentidad;

    @Column(name = "nombres", nullable = false, length = 50 )
    private String nombres;

    @Column(name = "apellidos", nullable = false, length = 50 )
    private String apellidos;

    @Column(name = "fechaN", nullable = false, length = 50 )
    private String fechaNac;

    @Column(name = "ciudad", nullable = false, length = 50 )
    private String ciudad;

    @Column(name = "direccionResi", nullable = false, length = 50 )
    private String direccion;

    @Column(name = "email", nullable = false, length = 50 )
    private String email;

    @Column(name = "telefonoF", nullable = false, length = 50 )
    private long telFijo;

    @Column(name = "celular", nullable = false, length = 50 )
    private long celular;

    @Column(name = "nombreAcudiente", nullable = false, length = 50 )
    private String acudiente;

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public long getNumeriIdentidad() {
        return numeriIdentidad;
    }

    public void setNumeriIdentidad(long numeriIdentidad) {
        this.numeriIdentidad = numeriIdentidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTelFijo() {
        return telFijo;
    }

    public void setTelFijo(long telFijo) {
        this.telFijo = telFijo;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }

    public String getAcudiente() {
        return acudiente;
    }

    public void setAcudiente(String acudiente) {
        this.acudiente = acudiente;
    }
}
