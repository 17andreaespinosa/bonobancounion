package com.elFuturoDelSaber.data.mapper;

public interface IMapper <I,O> {

    O map(I in);
}
